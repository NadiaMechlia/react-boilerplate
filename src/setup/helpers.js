// Helpers

/**
 * Render element or component by provided condition
 * @param {*} condition 
 * @param {*} renderFn 
 */
export function renderIf(condition, renderFn) {
    return condition ? renderFn() : null
}

/**
 * Duplicate object
 * @param {*} object 
 */
export function duplicate(object) {
    return Object.assign({}, object)
}

/**
 * Return empty string if value is null
 * @param {*} value 
 */
export function nullToEmptyString(value) {
    return value === null ? '' : value
}


/**
 * Check if object is empty
 * @param {*} obj 
 */
export function isEmpty(obj) {
    let name;
    for (name in obj) {
        if (obj.hasOwnProperty(name)) {
            return false;
        }
    }
    if (obj.constructor !== Object) {
        return false;
    }
    return true;
}