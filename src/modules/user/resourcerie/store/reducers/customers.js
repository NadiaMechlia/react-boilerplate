import { combineReducers } from 'redux';
import { RECEIVE_CUSTOMERS } from '../constants/ActionTypes';

/**
 * Update customers list state
 * @param  {} state=[]
 * @param  {} action
 */
const customers = (state = [], action) => {
    switch (action.type) {
        case RECEIVE_CUSTOMERS:
            return action.customers;
        default:
            return state;
    }
};

export default combineReducers({
    customers
});

export const getCustomers = (state) => state.customers;
