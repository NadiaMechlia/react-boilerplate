import axios from 'axios';
import { APP_URL_API } from '../../../setup/config/env'


const api = axios.create({
  baseURL: APP_URL_API,
});

export default api;