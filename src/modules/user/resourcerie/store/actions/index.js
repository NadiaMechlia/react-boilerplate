import * as types from '../constants/ActionTypes'
import customerServ from '../../api/customers'

/**
 * Receive customers
 * @param  {} customers
 */
const receiveCustomers = (customers) => ({
    type: types.RECEIVE_CUSTOMERS,
    customers
});

/**
 * Get all customers
 */
export const getAllCustomers = () => (dispatch) => {
    customerServ.getCustomerList().then((customers) => {
        dispatch(receiveCustomers(customers.data.customers));
    });
};