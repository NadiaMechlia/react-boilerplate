import api from '../../../common/api';

/**
 * Login
 * @param {*} loginData 
 */
const login = (loginData) => {
    return api.post('/api/login', loginData);
};


/**
 * Signup
 * @param {*} signupData 
 */
const signup = (signupData) => {
    return api.post('/api/signup', signupData);
};


export default {
    login,
    signup
};
