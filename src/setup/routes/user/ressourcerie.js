// App Imports
import Ressourcerie from '../../../modules/user/resourcerie/Ressourcerie'

// User ressourcerie routes
export const ressourcerie = {
    path: '/ressourcerie',
    component: Ressourcerie,
    auth: false
}