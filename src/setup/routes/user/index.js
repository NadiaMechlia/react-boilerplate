// App Imports
import * as ressourcerie from './ressourcerie'
import * as programs from './programs'

// User routes
const user = {
    ...ressourcerie,
    ...programs
}

export default user