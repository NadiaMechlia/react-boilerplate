import api from '../../../common/api';

/**
 * Get customer list
 */
const getCustomerList = () => {
    return api.get('/api/customers');
};

export default {
    getCustomerList
};
